package com.mastertech.exercicio;

public class Sorteador {

    private Dado dado;
    IO io = new IO();

    public Sorteador(Dado dado) {
        this.dado = dado;
    }

    public Dado getDado() {
        return dado;
    }

    public void setDado(Dado dado) {
        this.dado = dado;
    }

    public Integer jogarUmDado(Dado dadoEscolhido) {
        int resultado = dadoEscolhido.jogarDado();
        io.imprimeResultado(resultado, TipoJogo.UNICO);
        return resultado;
    }

    public Integer jogarTresDados(Dado dadoEscolhido) {
        int somaDados = 0;
        for (int i = 0; i < 3; i++) {
            somaDados = somaDados + jogarUmDado(dadoEscolhido);
        }
        io.imprimeResultado(somaDados, TipoJogo.TRESDADOS);
        return somaDados;
    }

    public void jogarTresGrupos(Dado dadoEscolhido) {
        for (int i = 0; i < 3; i++) {
            jogarTresDados(dadoEscolhido);
        }
    }

}
