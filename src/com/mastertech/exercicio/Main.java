package com.mastertech.exercicio;

public class Main {

    public static void main(String[] args) {

        Mesa mesaDeBar = new Mesa();
        Dado dado = new Dado(6);
        Sorteador player = new Sorteador(dado);
        mesaDeBar.setSorteador(player);

        mesaDeBar.iniciarJogoDeDados();

    }
}
