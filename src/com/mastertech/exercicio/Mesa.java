package com.mastertech.exercicio;

public class Mesa {

    private Sorteador sorteador;

    public Mesa() {
    }

    public void iniciarJogoDeDados(){
        IO io = new IO();
        io.apresentaMensagem("INICIANDO JOGO DE DADOS");
        Dado dadoEscolhido = this.sorteador.getDado();

        io.apresentaMensagem("Jogando apenas um dado...");
        sorteador.jogarUmDado(dadoEscolhido);

        io.apresentaMensagem("Jogando três dados...");
        sorteador.jogarTresDados(dadoEscolhido);

        io.apresentaMensagem("Jogando três grupos de três dados...");
        sorteador.jogarTresGrupos(dadoEscolhido);

    }

    public Sorteador getSorteador() {
        return sorteador;
    }

    public void setSorteador(Sorteador sorteador) {
        this.sorteador = sorteador;
    }



    public Mesa(Sorteador sorteador) {
        this.sorteador = sorteador;
    }
}
