package com.mastertech.exercicio;

public class IO {

    public void apresentaMensagem(String s) {
        System.out.println(s);
    }

    public void imprimeResultado(Integer i, TipoJogo tipoJogo) {
        if(tipoJogo == TipoJogo.UNICO){
            System.out.println("Lado sorteado: " + i);
        } else if (tipoJogo == TipoJogo.TRESDADOS) {
            System.out.println("Soma dos lados sorteados: " + i);
        }
    }

}
