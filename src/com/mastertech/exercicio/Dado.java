package com.mastertech.exercicio;

public class Dado {

    private Integer lados;

    public Dado(Integer lados) {
        this.lados = lados;
    }

    public Integer getLados() {
        return lados;
    }

    public void setLados(Integer lados) {
        this.lados = lados;
    }

    public Integer jogarDado(){

        int range = this.lados;
        int min = 1;

        //sorteio
        int resultado = (int)(Math.random() * range) + min;
        return resultado;
    }
}
